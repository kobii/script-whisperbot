if (window.WebSocket) {
    //---------------------------------
    //  Variables
    //---------------------------------
    var serviceUrl = "ws://127.0.0.1:3337/streamlabs";
    var socket = new WebSocket(serviceUrl);
    //---------------------------------
    //  Events
    //---------------------------------
    socket.onopen = function()
    {
        // Format your Authentication Information
        var auth = {
            author: "KobiQQ",
            website: "https://www.twitch.tv/kobiQQ",
            api_key: API_Key,
            events: [
                "EVENT_SHOW_SONG",
                "EVENT_CURRENCY_RELOAD",
                "EVENT_BIRTHDAY",
                "EVENT_UPDATE_VIEW_COUNT"
            ]
        };
        
        //  Send your Data to the server
        socket.send(JSON.stringify(auth));
        console.log("Connected");
    };

    socket.onerror = function (error) {
        //  Something went terribly wrong... Respond?!
        console.log("Error: " + error);
    };

    socket.onmessage = function (message) {

        var rawArray = JSON.parse(message.data);
        //console.log(jsonObject.event);
        //console.log(jsonObject);
        var array = JSON.parse(rawArray.data);

        /*
        if (rawArray.event === "EVENT_SHOW_SONG") {

            var songName = array.song;
            console.log(songName + " songname");
            changeSong(songName);

        }
        
        else if (rawArray.event === "EVENT_BIRTHDAY") {

            var userName = array.userName;
            console.log(userName + " username");
            birthday(userName);

        }
        */

        if (rawArray.event === "EVENT_UPDATE_VIEW_COUNT") {

            var viewCount = array.currentViewer;
            console.log(viewCount + " viewcount");
            updateViewCount(viewCount);

        }

        else if (rawArray.event === "EVENT_CURRENCY_RELOAD") {
            //  Pass Data Along
            var jsonData = JSON.parse(jsonObject.data);
            loadSettings(jsonData);
        }

    };

    socket.onclose = function () {
        //Connection has been closed by you or the server
        console.log("Connection Closed!");
    };   
}