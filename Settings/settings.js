﻿var settings = {
  "OnlyLive": true,
  "ShortInterval": 15,
  "MediumInterval": 90,
  "LongInterval": 120,
  "UseCD": true,
  "HydroMessage": "You should drink something! By now you should have drank {0} ml",
  "HydroAmount": 150,
  "HydroInterval": 3600,
  "setBirthDayCommand": "!birthday"
};