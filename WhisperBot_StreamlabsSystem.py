import sys
import json
import os
import codecs
import datetime
import operator
import clr
import time

clr.AddReference("IronPython.SQLite.dll")
clr.AddReference("IronPython.Modules.dll")

import sqlite3
import unicodedata
from datetime import datetime, timedelta 

clr.AddReferenceToFileAndPath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../Moduls/basicFunctionality/bin/Debug/netstandard2.0/basicFunctionality.dll"))
from basicFunctions import *

clr.AddReferenceToFileAndPath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../Moduls/twitchAPI/bin/Debug/netstandard2.0/twitchAPI.dll"))
from twitchRestAPI import *

#---------------------------------------
# [Required] Script information
#---------------------------------------

ScriptName = "WhisperBot"
Website = "https://www.twitch.tv/kobiqq"
Creator = "Gobal | KobiQQ"
Version = "1.2"
Description = "Automated messages"
# + Added music display and birthday check

#---------------------------------------
# Variables
#---------------------------------------

m_ConfigFile = os.path.join(os.path.dirname(__file__), "Settings/settings.json")
m_ConfigFileJs = os.path.join(os.path.dirname(__file__), "Settings/settings.js")

#---------------------------------------
# Classes Tries to load settings from file if given The 'default' variable names need to match UI_Config
#---------------------------------------
class Settings:
    """" Loads settings from file if file is found if not uses default values"""

    # The 'default' variable names need to match UI_Config
    def __init__(self, settingsFile=None):
        if settingsFile and os.path.isfile(settingsFile):
            with codecs.open(settingsFile, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')
         
        else: #set variables if no settings file
            self.OnlyLive = True
            self.ShortInterval = 15
            self.MediumInterval = 90
            self.LongInterval = 240
            self.HydroAmount = 150
            self.HydroInterval = 3600
            self.HydroMessage = "You should drink something! By now you should have drank {0} ml"
            self.setBirthDayCommand = "!setbirthday"

    # Reload settings on save through UI
    def ReloadSettings(self, data):
        """Reload settings on save through UI"""
        self.__dict__ = json.loads(data, encoding='utf-8-sig')
        return

    # Save settings to files (json and js)
    def SaveSettings(self, settingsFile):
        """Save settings to files (json and js)"""
        with codecs.open(settingsFile, encoding='utf-8-sig', mode='w+') as f:
            json.dump(self.__dict__, f, encoding='utf-8-sig')
        with codecs.open(settingsFile.replace("json", "js"), encoding='utf-8-sig', mode='w+') as f:
            f.write("var settings = {0};".format(json.dumps(self.__dict__, encoding='utf-8-sig', ensure_ascii=False)))
        return

MySettings = Settings()


#---------------------------------------
# [Required] functions
#---------------------------------------
def Init():
    """Required tick function"""
    # Globals
    global MySettings
    MySettings = Settings()


    if not os.path.isfile(m_ConfigFile):
        text_file = codecs.open(m_ConfigFile, encoding='utf-8-sig', mode='w')
        out = json.dumps(MySettings.__dict__, encoding="utf-8-sig")
        text_file.write(out)
        text_file.close()
    else:
        with codecs.open(m_ConfigFile,encoding='utf-8-sig', mode='r') as ConfigFile:
            MySettings.__dict__ = json.load(ConfigFile)

    if not os.path.isfile(m_ConfigFileJs):
        text_file = codecs.open(m_ConfigFileJs, encoding='utf-8-sig', mode='w')
        jsFile = "var settings =" + json.dumps(MySettings.__dict__, encoding="utf-8-sig") + ";"
        text_file.write(jsFile)
        text_file.close()


    global basicFuncs
    basicFuncs = getBasicFunctions()

    global twitchAPICalls
    twitchAPICalls = StandartAPICalls()

    global hydro 
    hydro = 0

    global botList
    botList = ["kobiqq","djkobi","commanderroot","lurxx","electricallongboard","anotherttvviewer","electricallongboard"]

    global lastTimestampShortInterval, lastTimestampMediumInterval, lastTimestampLongInterval, lastHydroCheck, VIPStartLast

    lastHydroCheck = time.time()
    VIPStartLast = time.time()

    lastTimestampShortInterval = time.time()
    lastTimestampMediumInterval = time.time()
    lastTimestampLongInterval = time.time()


    # End of Init
    return

def Execute(data):

    if data.IsChatMessage() and data.GetParam(0).lower() == MySettings.setBirthDayCommand.lower():
        

        dayList = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
        monthList = ['1','2','3','4','5','6','7','8','9','10','11','12']

        if data.GetParam(1) in dayList and data.GetParam(2) in monthList and (data.GetParam(3).isdigit() and int(data.GetParam(3)) > 1920 and int(data.GetParam(3)) < 2020):

            userName = Parent.GetDisplayName(data.User) 
            day = data.GetParam(1)
            month = data.GetParam(2)
            year = data.GetParam(3)

            insertBirtday(userName,day,month,year)
            
        else:
            basicFuncs.streamMessage(Parent,"Wrong format")

 

    return

def Tick():
    """Required tick function"""

    if (Parent.IsLive() or not MySettings.OnlyLive):

        global lastTimestampShortInterval, lastTimestampMediumInterval, lastTimestampLongInterval, VIPStartLast
        global lastHydroCheck

        #Short time Interval in the past used for Music
        #if time.time() - lastTimestampShortInterval > MySettings.ShortInterval:

            #lastTimestampShortInterval = time.time()              


        #Medium time Interval
        if time.time() - lastTimestampMediumInterval > MySettings.MediumInterval:

            lastTimestampMediumInterval = time.time()
            
            #Whisper viewers if they have more than 100 points
            whisperViewersIfEnoughPoints()

        #long Interval
        if time.time() - lastTimestampLongInterval > MySettings.LongInterval:

            lastTimestampLongInterval = time.time()

            if Parent.GetViewerList() != None:
                myDict = {}
                if Parent.GetViewerList() != 0:
                    checkBirtday()

            # Bug
            #updateStreamStatistics()

               
        if time.time() - lastHydroCheck > MySettings.HydroInterval:
            global hydro
            hydro += MySettings.HydroAmount
            #if bot is subscribed - string.format(kobiqqLove)
            
            basicFuncs.streamMessage(Parent,str(MySettings.HydroMessage.format(hydro)))
            lastHydroCheck = time.time()
            updateVIP()

        if time.time() - VIPStartLast > 900:

            updateVIPBonus()
            VIPStartLast = time.time()

    return

def updateStreamStatistics():

    kobiChannelID = "26197959"

    channelData = twitchAPICalls.channelData(Parent,str(kobiChannelID))

    currentGame = channelData['stream']['game']
    currentViewers = channelData['stream']['viewers']

    myDict = {"currentViewer":str(currentViewers)}
    Parent.BroadcastWsEvent("EVENT_UPDATE_VIEW_COUNT", json.dumps(myDict))
    #basicFuncs.clientEvent(Parent,"EVENT_UPDATE_VIEW_COUNT", str(myDict))
    

    conn = sqlite3.connect(os.path.dirname(__file__) + "/../globalFiles/Datenbanken/streamMetaData.db")
    c = conn.cursor()
    try:

        c.execute("SELECT game FROM gameStats WHERE id='1'")
        row = c.fetchone()
        oldGame = row[0]

        if oldGame != currentGame:
            #change alert bar ui icons
            pass

        c.execute("UPDATE gameStats SET game='" + str(currentGame) + "', viewer = '" + str(currentViewers) + "'WHERE id='1'")
        conn.commit()

    except:
        basicFuncs.printLog(Parent,str(ScriptName), "Error couldnt update Database statistics")

    finally:
        conn.close()

    return

def whisperViewer(viewers,case,userRank):

    conn = sqlite3.connect(os.path.dirname(__file__) + "/../globalFiles/Datenbanken/streamUserData.db")
    c = conn.cursor()

    try:
                            
        c.execute("SELECT id, name, whisperAttempts FROM whisperuser WHERE name='" + viewers + "'")
        row = c.fetchone()
        id  =  row[0]
        name = row[1]
        whisperAttempts = row[2]

        try:
            if(whisperAttempts != case):

                c.execute("UPDATE whisperuser SET whisperAttempts ='" + str(case) + "' WHERE name='" + viewers + "'")
                conn.commit()

                if(case == 2):
                    Parent.SendStreamWhisper(viewers,"You got more than 150 Points, you should do a level up. Use !quiz or !slot. Your rank is: " + userRank +".")
                elif(case == 3):
                    Parent.SendStreamWhisper(viewers,"You got more than 200 Points, use your Points before u do a level up. Use !quiz or !slot. Your rank is: " + userRank +".")
    
        except:
            pass
    except:

        c.execute("INSERT INTO whisperuser ('name','whisperAttempts') values ('" + viewers + "','" + str(case) + "') ")
        conn.commit()

        Parent.SendStreamWhisper(viewers,"You got 100 Points so you can make a level up. Use !quiz or !slot. Your rank is: " + userRank +".")

    finally:

        conn.close()

    viewers = None
    case = None
    userRank = None

    return

def checkBirtday():
    
    global now,date,modified_date

    from datetime import datetime, timedelta
    ##now = datetime.now()
    ##date = datetime.strptime(now, "%Y %m %d")
    ##modified_date = date + timedelta(days=1)
    ##datetime.strftime(modified_date, "%Y/%m/%d")

    ##################################
    ### havent used the code above
    #########################################


    for viewers in Parent.GetViewerList():
        userPoints = Parent.GetPoints(viewers)

    from datetime import date

    currentFullDate = datetime.now()


    todayDay = currentFullDate.day
    todayMonth = currentFullDate.month

    #Parent.Log(ScriptName,str(todayDay))
               
    conn = sqlite3.connect(os.path.dirname(__file__) + "/../globalFiles/Datenbanken/streamUserData.db")
    c = conn.cursor()

    c.execute("SELECT id FROM birthdays WHERE day='" + str(todayDay) + "' and month='" + str(todayMonth) + "' and gift='0' and username='" + viewers + "'")
    row = c.fetchone()
                
    if row is not None: 
        id  =  row[0]
        basicFuncs.printLog(Parent,str(ScriptName), "Birthday boy/girl found")

        userChatName = Parent.GetDisplayName(viewers) 
        Parent.AddPoints(viewers,userChatName,50)
        c.execute("UPDATE birthdays SET gift ='1' WHERE username='" + viewers + "'")


        basicFuncs.streamMessage(Parent,"Happy Birthday @" + str(userChatName) + "!")

        eventList = []
        varList = []

        eventList.append("EVENT_BIRTHDAY")
        variableDict = {"userName":userChatName}
        varList.append(variableDict)
        dict = {"eventList":eventList,"varList":varList} 

        Parent.BroadcastWsEvent("sendEvent", json.dumps(dict))   
        
        # Bug
        #basicFuncs.clientEvent(Parent,"EVENT_BIRTHDAY",  str(myDict))

        conn.commit()

    conn.close()


    return

def insertBirtday(userName,day,month,year):


    conn = sqlite3.connect(os.path.dirname(__file__) + "/../globalFiles/Datenbanken/streamUserData.db")
    c = conn.cursor()

    try:
        c.execute("SELECT id FROM birthdays WHERE username='" + userName + "'")
        row = c.fetchone()

        id = row[0]

        Parent.SendStreamWhisper(userName,"You have allready inserted your Birthday")
    
            
    except:
        c.execute("INSERT INTO birthdays ('username','day','month','year') values ('" + str(userName) + "','" + str(day) + "','" + str(month) + "','" + str(year) + "') ")
        conn.commit()
        
        Parent.SendStreamWhisper(userName,"Birtday inserted")

    finally:
        conn.close()

    return

#Vip points for people in chat
def updateVIP():

    conn = sqlite3.connect(os.path.dirname(__file__) + "/../globalFiles/Datenbanken/streamMetaData.db")
    c = conn.cursor()

    for viewers in Parent.GetViewerList():

        if viewers not in botList:

            try:
                c.execute("SELECT VIPPoints FROM viptracking WHERE name='" + str(viewers) + "'")
                row = c.fetchone()

                VIPPoints = row[0]

                VIPPoints +=1

                c.execute("UPDATE viptracking SET VIPPoints ='" + str(VIPPoints) + "' WHERE name='" + str(viewers) + "'")
                conn.commit()   
            
            except:
                c.execute("INSERT INTO viptracking ('name','VIPPoints') values ('" + str(viewers) + "','1') ")
                conn.commit()

            finally:
                viewers = None
                
    conn.close()

    return

#Vip points for ACTIVE people in chat
def updateVIPBonus():

    conn = sqlite3.connect(os.path.dirname(__file__) + "/../globalFiles/Datenbanken/streamMetaData.db")
    c = conn.cursor()
                
    for activeViewer in Parent.GetActiveUsers():

        if activeViewer not in botList:

            try:
                c.execute("SELECT VIPPoints FROM viptracking WHERE name='" + str(activeViewer) + "'")
                row = c.fetchone()

                VIPPoints = row[0]

                VIPPoints +=2

                c.execute("UPDATE viptracking SET VIPPoints ='" + str(VIPPoints) + "' WHERE name='" + str(activeViewer) + "'")
                conn.commit()   
            
            except:
                c.execute("INSERT INTO viptracking ('name','VIPPoints') values ('" + str(activeViewer) + "','2') ")
                conn.commit()

            finally:
                activeViewer = None

    conn.close()

    return

#automatic update VIP once per x weeks 
def removeOldVIPS():

    return

#automatic update VIP once per x weeks 
def addNewVIPS():

    return


def whisperViewersIfEnoughPoints():

    myDict = {}
    for viewers in Parent.GetViewerList():
        userPoints = Parent.GetPoints(viewers)

        userRank = str(Parent.GetRank(viewers))
        userRankLevel = userPoints/10000
        rankUp = 10000
        userChatName = Parent.GetDisplayName(viewers)

        if userRankLevel == 0:
            rightUserPoints = userPoints

        else:            
            rightUserPoints = userPoints - (userRankLevel * 10000)

        if rightUserPoints > 100 and rightUserPoints < 150:
 
            myDict[viewers] = rightUserPoints
            whisperViewer(viewers,1,userRank)

        elif rightUserPoints > 150 and rightUserPoints < 200:
                        
            myDict[viewers] = rightUserPoints
            whisperViewer(viewers,2,userRank)

        elif rightUserPoints > 200 and rightUserPoints < 250:
                        
            myDict[viewers] = rightUserPoints
            whisperViewer(viewers,3,userRank)

        else:
            pass

    return

#Reload Settings
def ReloadSettings(jsonData):
    MySettings.__dict__ = json.loads(jsonData)
    Parent.BroadcastWsEvent("EVENT_CURRENCY_RELOAD", jsonData)
    return

#---------------------------------------
# Reload Settings on Save
#---------------------------------------
def ReloadSettings(jsonData):
    # Globals
    global MySettings

    # Reload saved settings
    MySettings.ReloadSettings(jsonData)

    # End of ReloadSettings
    return

def UpdateSettings():
    with open(m_ConfigFile) as ConfigFile:
        MySettings.__dict__ = json.load(ConfigFile)
    return
